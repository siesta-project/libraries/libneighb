!
program test
  
  use neighbour, only: mneighb, jna=>jan, xij, r2ij

  implicit none

  integer, parameter :: dp = selected_real_kind(10,100)
  
  integer, parameter :: NA = 2
  real(dp), parameter :: alat = 4.0_dp

  real(dp) :: cell(3,3), xa(3,NA)
  integer :: ia, jn, nnia

  cell = reshape([ 1.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 1.0_dp, 0.0_dp, &
       0.0_dp, 0.0_dp, 1.0_dp], [3,3])
  cell = alat * cell

  xa(:,1) = [ 0.25_dp, 0.25_dp, 0.25_dp ] * alat
  xa(:,2) = [ 0.75_dp, 0.75_dp, 0.75_dp ] * alat
  

  !Initialize
  call mneighb( cell, 2.0_dp*alat, NA, xa, 0, 0, nnia )

  do ia = 1,NA

     print "(a,i3)", "Neighbors of atom ", ia
     call mneighb( cell, 2.0_dp*alat, NA, xa, ia, 0, nnia )
     do jn = 1,nnia
         print "(a20,f10.5)", "r2ij: ", r2ij(jn)
         print "(a20,3f10.5)", "xij: ", xij(:,jn)
      enddo
   enddo
!      call reset_neighbour_arrays( )
 end program test
